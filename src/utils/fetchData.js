import axios from 'axios'

const fetchData = ({ url, method = 'GET', data = {}}) => {
  return axios({
    url,
    method,
    data,
  })
}

export default fetchData
