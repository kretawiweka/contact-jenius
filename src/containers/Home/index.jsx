import React, { useEffect, useState } from 'react'
import { Link } from "react-router-dom"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Skeleton from 'react-loading-skeleton';

import AddIcon from '@material-ui/icons/Add'
import Fab from '@material-ui/core/Fab'

import ListItem from '../../components/ListItem'
import fetchData from '../../utils/fetchData'
import isEmpty from '../../utils/isEmpty'
import {
  AddButtonWrapper
} from './style'


const Home = () => {
  const [contactData, setContactData] = useState(null)
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    const fetchContactData = async () => {
      const data = await fetchData({
        url:`${process.env.REACT_APP_BASE_API}/contact`
      }).catch((err) => {
        setIsLoading(false)
        toast.error(err.response.data.message ? err.response.data.message : "Something Error")
        throw err
      })
      setContactData(data.data.data)
      setIsLoading(false)
    }
    fetchContactData()
  }, [])

  const deleteContact = async (dataItem) => {
    const res  = await fetchData({
      method: 'DELETE',
      url:`${process.env.REACT_APP_BASE_API}/contact/${dataItem.id}`
    }).catch((err) => {            
      setIsLoading(false)
      toast.error(err.response.data.message ? err.response.data.message : "Something Error")
      throw err
    })
    if(!isEmpty(res)) {
      if(res.status === 202){
        toast.success(`${dataItem.firstName} ${dataItem.lastName} deleted`)
      } else {
        toast.error("Something Error")                
      }
  } else {
      toast.error("Something Error")
  }
  }

  return (
    <React.Fragment>
      <ToastContainer/>
      {
        isLoading && <Skeleton count={3} height={50}/>
      }
      {contactData !== null && 
        contactData.map((dataItem, dataIndex) => (
          <ListItem key={dataIndex} data={dataItem} deleteFunc={()=>{deleteContact(dataItem)}}/>
        ))
      }
      <AddButtonWrapper>
        <Link to="/create">
          <Fab color="primary" aria-label="add">
            <AddIcon />
          </Fab>
        </Link>
      </AddButtonWrapper>
    </React.Fragment>
  )
}



export default Home
