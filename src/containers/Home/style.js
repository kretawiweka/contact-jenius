import styled from 'styled-components'

const AddButtonWrapper = styled.div`
    position: fixed;
    bottom: 14px;
    display: flex;
    max-width: 480px;
    width: 100%;
    justify-content: flex-end
`

export {
    AddButtonWrapper
}