import React, { useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import {
    ButtonWrapper,
    FormWrapper
} from '../../components/Form'
import fetchData from '../../utils/fetchData'
import isEmpty from '../../utils/isEmpty';

const CreateContact = () => {
    const [contactData, setContactData] = useState({})

    const createContact = async (event) => {
        event.preventDefault()
        const res = await fetchData({
            method: 'POST',
            url:`${process.env.REACT_APP_BASE_API}/contact`,
            data: contactData
        }).catch((err) => {
            toast.error(err.response.data.message ? err.response.data.message : "Something Error")
            throw err
        })
        if(!isEmpty(res)) {
            if(res.status === 201){
                toast.success(`Success save ${contactData.firstName} ${contactData.lastName}`)
            } else {
                toast.error("Something Error")                
            }
        } else {
            toast.error("Something Error")
        }
    }

    const handleChange = (event) => {
        setContactData({
            ...contactData,
            [event.target.name]: event.target.value
        })
    }

    return (
        <React.Fragment>
            <ToastContainer/>
            <Typography variant="h6">
                Create Contact
            </Typography>
            <FormWrapper>
                <form onSubmit={createContact}>
                    <TextField name="firstName" onChange={handleChange} label="First Name" margin="dense" fullWidth required/>
                    <TextField name="lastName" onChange={handleChange} label="Last Name" margin="dense" fullWidth required/>
                    <TextField name="age" onChange={handleChange} label="Age" type="number" margin="dense" fullWidth required/>
                    <TextField name="photo" onChange={handleChange} label="Photo" margin="dense" fullWidth required/>
                    <ButtonWrapper>
                        <Button variant="contained" color="primary" fullWidth type="submit">
                            Create
                        </Button>
                    </ButtonWrapper>
                </form>
            </FormWrapper>
        </React.Fragment>
    )
}

export default CreateContact