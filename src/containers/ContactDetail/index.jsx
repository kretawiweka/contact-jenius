import React, { useEffect, useState } from 'react'
import { useParams } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Skeleton from 'react-loading-skeleton';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import {
    ButtonWrapper,
    FormWrapper
} from '../../components/Form'

import fetchData from '../../utils/fetchData'
import isEmpty from '../../utils/isEmpty';

const ContactDetail = props => {
    const { contactId } = useParams();
    const [ contactData, setContactData ] = useState(null)
    const [ isLoading, setIsLoading ] = useState(true)

    useEffect(() => {
        const getContactDetail = async () => {
            const data = await fetchData({
                method: 'GET',
                url:`${process.env.REACT_APP_BASE_API}/contact/${contactId}`
            }).catch((err) => {
                setIsLoading(false)
                toast.error(err.response.data.message ? err.response.data.message : "Something Error")
                toast.error("Something Error")
            })
            setContactData(data.data.data)
            setIsLoading(false)
        }
        getContactDetail()
    }, [])

    const handleChange = (event) => {
        setContactData({
            ...contactData,
            [event.target.name]: event.target.value
        })
    }

    const updateContact = async (event) => {
        event.preventDefault()
        const res = await fetchData({
            method: 'PUT',
            url:`${process.env.REACT_APP_BASE_API}/contact/${contactId}`,
            data: {
                firstName: contactData.firstName,
                lastName: contactData.lastName,
                age: contactData.age,
                photo: contactData.photo
            }
        }).catch((err) => {
            setIsLoading(false)
            toast.error(err.response.data.message ? err.response.data.message : "Something Error")
            throw err
        })
        if(!isEmpty(res)) {
            if(res.status === 201){
                toast.success(`Success update ${contactData.firstName} ${contactData.lastName}`)
            } else {
            toast.error("Something Error")                
            }
        } else {
            toast.error("Something Error")
        }
    }

    return (
        <React.Fragment>
            <Typography variant="h6">
                Contact Detail
            </Typography>
            <ToastContainer/>
            {
                isLoading && <Skeleton count={4} height={25}/>
            }
            {contactData !== null && 
                <FormWrapper>
                    <form onSubmit={updateContact}>
                        <TextField value={contactData.firstName} name="firstName" onChange={handleChange} label="First Name" margin="dense" fullWidth required/>
                        <TextField value={contactData.lastName} name="lastName" onChange={handleChange} label="Last Name" margin="dense" fullWidth required/>
                        <TextField value={contactData.age} name="age" onChange={handleChange} label="Age" type="number" margin="dense" fullWidth required/>
                        <TextField value={contactData.photo} name="photo" onChange={handleChange} label="Photo" margin="dense" fullWidth required/>
                        <ButtonWrapper>
                            <Button variant="contained" color="primary" fullWidth type="submit">
                                Update
                            </Button>
                        </ButtonWrapper>
                    </form>
                </FormWrapper>
            }
        </React.Fragment>
    )
}

export default ContactDetail