import styled from 'styled-components'

const ButtonWrapper = styled.div`
    margin-top: 35px;
`

const FormWrapper = styled.div`
    padding: 14px;
`

export {
    ButtonWrapper,
    FormWrapper
}