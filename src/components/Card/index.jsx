import React from 'react'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'

import {
  CardWrapper,
  Image
} from './style'

const CardComponent = () =>  {
  return (
    <CardWrapper>
      <Card>
        <CardHeader
          title="Bilbo Baggins"
        />
        <CardMedia
          image="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"
          title="Paella dish"
        />
        <CardContent>
          <Image src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"/>
          <Typography variant="body2" color="textSecondary" component="p">
          This impressive paella is a perfect party dish and a fun meal to cook together with your
          guests. Add 1 cup of frozen peas along with the mussels, if you like.
          </Typography>
        </CardContent>
      </Card>
    </CardWrapper>
  )
}

export default CardComponent
