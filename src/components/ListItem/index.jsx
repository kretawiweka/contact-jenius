import React from 'react'
import { Link } from "react-router-dom"
import Divider from '@material-ui/core/Divider';
import DeleteIcon from '@material-ui/icons/DeleteOutline';
import Typography from '@material-ui/core/Typography';

import {
    Wrapper,
    TitleWrapper
} from './style'

const ListItem = props => {
    return (
        <React.Fragment>
            <Wrapper>
                <Link to={`/contact/${props.data.id}`}>
                    <TitleWrapper>
                        <Typography variant="body1">
                            {props.data.firstName} {props.data.lastName}
                        </Typography>
                    </TitleWrapper>
                </Link>
                <DeleteIcon onClick={props.deleteFunc}/>
            </Wrapper>
            <Divider/>
        </React.Fragment>
    )
}

export default ListItem