import styled from 'styled-components'

const Wrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    cursor: pointer;
    padding: 0px 7px;
    :hover {
        background-color: #f5f5f5;
    }
    a {
        text-decoration: none !important;   
        color: #000000;
        width: 100%;
    }
`

const TitleWrapper = styled.div`
    flex: 1;
    padding: 10px 0px;

`

export {
    Wrapper,
    TitleWrapper
}