import styled from 'styled-components'

const MainContainer = styled.div`
  padding: 15px;
  background-color: #FFFFFF;
  border: 1px solid #F5F5F5;
  max-width: 480px;
  margin: auto;
  min-height: calc(100vh - 70px);
`

export {
  MainContainer
}
