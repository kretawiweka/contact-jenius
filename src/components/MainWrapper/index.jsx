import React from 'react'
import { MainContainer } from './style'

const MainWrapper = props => {
  return (
    <MainContainer>
      {props.children}
    </MainContainer>
  )
}

export default MainWrapper
