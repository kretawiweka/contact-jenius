import styled from 'styled-components'

const HeaderText = styled.h1`
  margin: 14px 28px;
  padding: 0px;
  font-size: 24px;
  font-weight: 500;
`

export {
  HeaderText
}
