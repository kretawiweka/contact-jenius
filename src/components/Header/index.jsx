import React from 'react'
import { AppBar } from '@material-ui/core'
import {
  HeaderText
} from './style'

const Header = () => {
  return (
    <AppBar color="default" position="static">
      <HeaderText>Contact Jenius</HeaderText>
    </AppBar>
  )
}

export default Header
