import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import Home from './page/home'
import ContactDetail from './page/contact-detail'
import CreateContact from './page/create'
const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path='/'>
          <Home />
        </Route>
        <Route path="/contact/:contactId">
          <ContactDetail/>
        </Route>
        <Route path="/create">
          <CreateContact/>
        </Route>
      </Switch>
    </Router>
  )
}

export default App