import React from 'react'

import Header from '../../components/Header'
import MainWrapper from '../../components/MainWrapper'
import HomeContainer from '../../containers/Home'

const Home = () => {
  return (
    <React.Fragment>
      <Header/>
      <MainWrapper>
        <HomeContainer/>
      </MainWrapper>
    </React.Fragment>
  )
}

export default Home
