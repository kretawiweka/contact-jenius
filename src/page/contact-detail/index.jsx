import React from 'react'

import Header from '../../components/Header'
import MainWrapper from '../../components/MainWrapper'
import ContactDetailContainer from '../../containers/ContactDetail'

const ContactDetail = () => {
  return (
    <React.Fragment>
      <Header/>
      <MainWrapper>
        <ContactDetailContainer/>
      </MainWrapper>
    </React.Fragment>
  )
}

export default ContactDetail
