import React from 'react'

import Header from '../../components/Header'
import MainWrapper from '../../components/MainWrapper'
import CreateContactContainer from '../../containers/CreateContact'

const CreateContact = () => {
  return (
    <React.Fragment>
      <Header/>
      <MainWrapper>
        <CreateContactContainer/>
      </MainWrapper>
    </React.Fragment>
  )
}

export default CreateContact
